package com.alialmasli.mobilecalls;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

class SocketListener extends WebSocketListener {

    private static final String TAG = "SocketListener";

    private Context context;
    private TextView response;

    SocketListener(Context context, TextView response) {
        this.context = context;
        this.response = response;
    }

    @Override
    public void onClosed(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
        super.onClosed(webSocket, code, reason);
        Log.d(TAG, "onClosed: -> " + reason);
    }

    @Override
    public void onClosing(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
        super.onClosing(webSocket, code, reason);
        Log.d(TAG, "onClosing: -> " + reason);
    }

    @Override
    public void onFailure(@NotNull WebSocket webSocket, @NotNull Throwable t, @Nullable Response response) {
        super.onFailure(webSocket, t, response);
        Log.d(TAG, "onFailure: -> " + t.getMessage());
    }

    @Override
    public void onMessage(@NotNull WebSocket webSocket, @NotNull final String text) {
        super.onMessage(webSocket, text);

        Log.d(TAG, "onMessageByText: -> " + text);

        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{
                    response.setText(text);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onMessage(@NotNull WebSocket webSocket, @NotNull ByteString bytes) {
        super.onMessage(webSocket, bytes);
        Log.d(TAG, "onMessageByByte: -> " + bytes.toString());
    }

    @Override
    public void onOpen(@NotNull WebSocket webSocket, @NotNull Response response) {
        super.onOpen(webSocket, response);

        Log.d(TAG, "onOpen: -> " + response.toString());
        
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, "Connection Established", Toast.LENGTH_SHORT).show();
            }
        });
    }
}