package com.alialmasli.mobilecalls.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

public class GetContactName {
    private static final String TAG = "GetContactName";

    public static String getContactName(Context context, String number) {

        String name = null;

        String[] projection = new String[] {
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup._ID};

        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);

        if(cursor != null) {
            if (cursor.moveToFirst()) {
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                Log.v(TAG, "Started: Contact number = " + number);
                Log.v(TAG, "Started: Contact name  = " + name);
            } else {
                Log.v(TAG, "Contact Not Found = " + number);
            }
            cursor.close();
        }
        return name;
    }

}